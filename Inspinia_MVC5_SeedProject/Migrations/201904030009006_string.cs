namespace Inspinia_MVC5_SeedProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _string : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cursoes", "CargaHoraria", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cursoes", "CargaHoraria", c => c.DateTime(nullable: false));
        }
    }
}
