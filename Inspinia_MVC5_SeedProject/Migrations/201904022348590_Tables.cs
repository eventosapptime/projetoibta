namespace Inspinia_MVC5_SeedProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Alunoes",
                c => new
                    {
                        AlunoID = c.Int(nullable: false, identity: true),
                        nome = c.String(),
                        Cidade = c.String(),
                        CEP = c.String(),
                    })
                .PrimaryKey(t => t.AlunoID);
            
            CreateTable(
                "dbo.Contratoes",
                c => new
                    {
                        ContratoID = c.Int(nullable: false, identity: true),
                        AlunoID = c.Int(nullable: false),
                        CursoID = c.Int(nullable: false),
                        DataContrato = c.DateTime(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValorTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ContratoID)
                .ForeignKey("dbo.Alunoes", t => t.AlunoID, cascadeDelete: true)
                .ForeignKey("dbo.Cursoes", t => t.CursoID, cascadeDelete: true)
                .Index(t => t.AlunoID)
                .Index(t => t.CursoID);
            
            CreateTable(
                "dbo.Cursoes",
                c => new
                    {
                        CursoID = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        ValorTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CargaHoraria = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CursoID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contratoes", "CursoID", "dbo.Cursoes");
            DropForeignKey("dbo.Contratoes", "AlunoID", "dbo.Alunoes");
            DropIndex("dbo.Contratoes", new[] { "CursoID" });
            DropIndex("dbo.Contratoes", new[] { "AlunoID" });
            DropTable("dbo.Cursoes");
            DropTable("dbo.Contratoes");
            DropTable("dbo.Alunoes");
        }
    }
}
