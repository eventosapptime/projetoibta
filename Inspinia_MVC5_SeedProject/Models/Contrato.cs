﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inspinia_MVC5_SeedProject.Models
{
    public class Contrato
    {
        [Key]
        public int ContratoID { get; set; }
        public int AlunoID { get; set; }
        [ForeignKey("AlunoID")]
        public virtual Aluno Aluno { get; set; }
        public int CursoID { get; set; }
        [ForeignKey("CursoID")]
        public virtual Curso Curso { get; set; }
        public DateTime DataContrato { get; set; }
        public decimal Valor { get; set; }
        public decimal ValorTotal { get; set; }
    }
}