﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inspinia_MVC5_SeedProject.Models
{
    public class Curso
    {
        [Key]
        public int CursoID { get; set; }
        public string Nome { get; set; }
        public decimal ValorTotal { get; set; }
        public String CargaHoraria { get; set; }
         
    }
}