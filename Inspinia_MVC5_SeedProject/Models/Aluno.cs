﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inspinia_MVC5_SeedProject.Models
{
    public class Aluno
    {
        [Key]
        public int AlunoID { get; set; }
        public string nome { get; set; }
        public string Cidade { get; set; }
        public string CEP { get; set; }

    }
}