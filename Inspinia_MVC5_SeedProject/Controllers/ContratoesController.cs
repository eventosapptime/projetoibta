﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Inspinia_MVC5_SeedProject.Models;

namespace Inspinia_MVC5_SeedProject.Controllers
{
    public class ContratoesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Contratoes/
        public ActionResult Index()
        {
            var contrato = db.Contrato.Include(c => c.Aluno).Include(c => c.Curso);
            return View(contrato.ToList());
        }

        // GET: /Contratoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contrato contrato = db.Contrato.Find(id);
            if (contrato == null)
            {
                return HttpNotFound();
            }
            return View(contrato);
        }

        // GET: /Contratoes/Create
        public ActionResult Create()
        {
            ViewBag.AlunoID = new SelectList(db.Aluno, "AlunoID", "nome");
            ViewBag.CursoID = new SelectList(db.Curso, "CursoID", "Nome");
            return View();
        }

        // POST: /Contratoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ContratoID,AlunoID,CursoID,DataContrato,Valor,ValorTotal")] Contrato contrato)
        {
            if (ModelState.IsValid)
            {
                db.Contrato.Add(contrato);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AlunoID = new SelectList(db.Aluno, "AlunoID", "nome", contrato.AlunoID);
            ViewBag.CursoID = new SelectList(db.Curso, "CursoID", "Nome", contrato.CursoID);
            return View(contrato);
        }

        // GET: /Contratoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contrato contrato = db.Contrato.Find(id);
            if (contrato == null)
            {
                return HttpNotFound();
            }
            ViewBag.AlunoID = new SelectList(db.Aluno, "AlunoID", "nome", contrato.AlunoID);
            ViewBag.CursoID = new SelectList(db.Curso, "CursoID", "Nome", contrato.CursoID);
            return View(contrato);
        }

        // POST: /Contratoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ContratoID,AlunoID,CursoID,DataContrato,Valor,ValorTotal")] Contrato contrato)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contrato).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AlunoID = new SelectList(db.Aluno, "AlunoID", "nome", contrato.AlunoID);
            ViewBag.CursoID = new SelectList(db.Curso, "CursoID", "Nome", contrato.CursoID);
            return View(contrato);
        }

        // GET: /Contratoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contrato contrato = db.Contrato.Find(id);
            if (contrato == null)
            {
                return HttpNotFound();
            }
            return View(contrato);
        }

        // POST: /Contratoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contrato contrato = db.Contrato.Find(id);
            db.Contrato.Remove(contrato);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
